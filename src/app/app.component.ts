import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-first-project';
  nombre = 'Oswaldo'
  edad = 15
  email = 'oswaldo.naupa@tecsup.edu.pe'
  sueldos = [1700,1600,1900];
  activo = true;
  sitio='http://www.google.com';
  profesion=''
  anios='';
}
